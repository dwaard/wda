<!DOCTYPE html>
<html>
	<head><title>World Domination Application</title></head>
	<body>
    <h1>WDA Restricted Area</h1>
    <?php
      // Open the database
    	$url = 'localhost';
    	$userid = 'wda';
    	$password = 'wda';
    	$database = 'wda';
    	$mysqli = new mysqli($url, $userid, $password, $database);
    	if ($mysqli -> connect_errno) {
    		die("Failed to connect to MySQL: (" . $mysqli -> connect_errno . ") " . $mysqli -> connect_error);
    	}

    	// Build the SQL query
    	$sql = "SELECT COUNT(*) AS NumberOfUSers FROM User";

    	// Send the query to the database
    	if (!$result = $mysqli -> query($sql)) {
    		die('There was an error running the query [' . $mysqli -> error . ']');
    	}

      // If there is a first row, read it. It should read the count of users in the User table
    	if ($row = $result -> fetch_assoc()) {
        // Read the number of users from the resultset
        $numberOfUsers = $row["NumberOfUSers"];

        if ($numberOfUsers == 1) {
          echo 'There is only one user';
        } else {
          echo 'There are '.$numberOfUsers.' users';
        }
        echo ' registered in this application.';
    	}
    ?>
		<form action="login.php" method="post" style="width: 450px; margin-left: auto; margin-right: auto">
			<h1>Please login</h1>
			<table >
				<tr>
					 <td valign="top">
					  	<label for="userid">Username</label>
					 </td>
					 <td valign="top">
					  	<input  type="text" name="userid" maxlength="50" size="30">
					 </td>
				</tr>
				<tr>
					 <td valign="top">
					  	<label for="passwd">Password</label>
					 </td>
					 <td valign="top">
					  	<input  type="password" name="passwd" value="" maxlength="50" size="30">
					 </td>
				</tr>
				<tr>
					 <td valign="top"> </td>
					 <td valign="top">
					  	<input type="submit" value="Login">
					 </td>
				</tr>
			</table>
		</form>
	</body>
</html>

<?php
/*
 * Here are some convenient functions one might use in this application.
 */

 /*
  * Checks whether the user is logged in.
  * When logged in, the server has a session stored for the user.
  */
function isLoggedIn() {
  return isset($_SESSION['logged_in']);
}

/*
 * Makes sure that the user is lgged in from now on, by setting some
 * data in the user session.
 */
function loginUser($username) {
  $_SESSION['logged_in'] = true;
  $_SESSION['username'] = $username;
}

/*
 * Logges off the user by destroying the session.
 */
 function logOff() {
   //Destroy the session
 	session_unset();
 	session_destroy();
 }

 ?>

# README #

Hello world!

Simple app to demonstrate php and mysql collaboration.

## How to install ##
1. clone this repo in your DocumentRoot
2. use database.sql to restore the database in MySQL
3. make a user with credentials in MySQL an grant him rights to this database
4. make sure the login credentials in index.php match the created user credentials
5. test the site, and try to login

<?php
// Include the code of functions.php in this script
include_once('functions.php');
// Make or fetch session data
session_start();

if (!isLoggedIn()) {
	header("HTTP/1.1 401 Unauthorized");
	echo 'You don’t exist. Go away.';
	exit;
}
?>

<!DOCTYPE html>
<html>
	<head><title>Secret Page</title></head>
	<body>
		<h1>Welcome to the World Domination Application</h1>
		<p>Welcome <?php echo $_SESSION['username']; ?></p>
		<p>
			<?php
				if(isset($_GET['action'])) {
					echo "<p>";
					if ($_GET['action'] === 'zombie') {
						echo "Zombie virus released. Have a nice day.";
					} else if ($_GET['action'] === 'disaster') {
						echo "Melt-inator worked fine. Please notice a nice rising of the sea level.";
					} else if ($_GET['action'] === 'puppy') {
						echo "The warm strokes on the soft furry puppy was a satisfying alternative for world domination.";
					} else {
						echo $_GET['action']." is not known to us. Are you hacking our system?";
					}
					echo "</p>";
				}
			 ?>
		</p>
		<form method="get" style="width: 450px; margin-left: auto; margin-right: auto">
			<h1>Select your action</h1>
	  		<input type="radio" name="action" value="zombie" checked>Release zombievirus</input>
  			<br>
  			<input type="radio" name="action" value="disaster">Melt polar icecaps with Melt-inator</input>
			<br>
  			<input type="radio" name="action" value="puppy">Save a puppy</input>
  			<br/>
			<input type="submit" value="Confirm">
		</form>
		<p/>
		<form action="logoff.php" style="width: 450px; margin-left: auto; margin-right: auto">
			<input type="submit" value="Logoff">
		</form>
	</body>
</html>
